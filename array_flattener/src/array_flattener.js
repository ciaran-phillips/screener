module.exports = {
    flatten
}

/**
 * Flattens a nested integer array 
 * 
 * @param {array} values 
 *      Array of integers or nested int arrays
 * 
 * @return {int[]}
 *      Flattened array of integers
 * 
 * @throws {TypeError}
 *      Will throw an error if the input contains anything other
 *      than nested integer arrays, or if the array is recursive
 */
function flatten(values) {
    if (!Array.isArray(values)) {
        throw new TypeError("Input must be an integer array");
    }
    const flattenedArray = [];
    depthFirstTraversal(values, flattenedArray);
    return flattenedArray;
};

/**
 * Flattens the array in O(n) time, with O(h) space complexity given h as the
 * depth of the nesting. No return value - operates on the flattenedArray argument
 * 
 * @param {array} element
 *      Array that can contain ints or nested arrays
 * @param {int[]} flattenedArray
 *      The flattened array so far, should be passed as empty to begin with
 * @param {Set} parents
 *      Set of parent arrays, used to identify recursion
 */
function depthFirstTraversal(element, flattenedArray, parents = new Set()) {
    if (!isValidElement(element)) {
        throw new TypeError("All elements must be integers or arrays");
    }
    if (Number.isInteger(element)) {
        flattenedArray.push(element);
    }
    else if (parents.has(element)) {
        throw new TypeError("Cannot flatten recursive arrays");
    }
    else {
        element.forEach((subElement) => {
            // Track the parents of current element, 
            // so that we can identify recursive arrays
            parents.add(element);
            depthFirstTraversal(subElement, flattenedArray, parents);
            parents.delete(element);
        });
    }
}

/**
 * Validates an element is either an array or an integer
 * 
 * @param {*} element
 *      element we want to validate
 * 
 * @return {boolean}
 */
function isValidElement(element) {
    return Array.isArray(element) || Number.isInteger(element);
}



