const assert = require('assert');

const array_flattener = require('../src/array_flattener.js');

describe('array_flattener', function () {
    describe('#flatten()', function () {
        const validInputCases = getValidInput();
        validInputCases.forEach(testValidInputCase);

        function testValidInputCase(test) {
            it(test.case, function() {
                const result = array_flattener.flatten(test.argument);
                assert.deepStrictEqual(result, test.expected);
            });
        }

        const badInputCases = getInvalidInput();
        badInputCases.forEach(testInvalidInputCase);

        function testInvalidInputCase(test) {
            it('should throw exception on invalid input: ' + test.case, function() {
                test.arguments.forEach(function(argument) {
                    assert.throws(() => {
                        array_flattener.flatten(argument);
                    }, TypeError);
                });
            });
        }
    });
});

/**
 * Gets a list of valid input cases
 * 
 * @return {Array<Object>}
 *   Return object consists of:
 *      object.case - string describing the case we're testing
 *      object.argument - argument to be passed to array flattener
 *      object.expected - expected result
 */
function getValidInput() {
    return [
        {
            case: "should have no effect on empty arrays", 
            argument: [],
            expected: []
        },
        {
            case: "should have no effect on flat arrays",
            argument: [2, 4, 7, 8],
            expected: [2, 4, 7, 8]
        },
        {
            case: "should flatten nested arrays",
            argument: [[[3]], 4, 7, [[4, 6, 7]]],
            expected: [3, 4, 7, 4, 6, 7]
        },
        {
            case: "should ignore empty nested arrays",
            argument: [[[]], 4, 7, [], [[4, 6, 7]]],
            expected: [4, 7, 4, 6, 7]
        }
    ];
}

/**
 * Gets a list of invalid input cases
 * 
 * @return {Array<Object>}
 *   Return object consists of:
 *      object.case - string describing the case we're testing
 *      object.arguments - list of arguments to be passed to array flattener 
 *          (separate examples that all match the case described in object.case)
 */
function getInvalidInput() {
    const badInputCases = [
            {
                case: "should throw exception on null values", 
                arguments: [[2, 4, null, 5, 6]]
            },
            {
                case: "should throw exception on nested null values",
                arguments: [
                    [[2, 4, 5], 6, 7, [3, null]]
                ]
            },
            {
                case: "should throw exception on non-array inputs",
                arguments: [
                    "myString",
                    54,
                    {},
                    null,
                    undefined
                ]
            },
            {
                case: "should throw exception on non-integer/array elements",
                arguments: [
                    ["myString"],
                    [54.5],
                    [[2, 5, 4.5]],
                    [undefined]
                ]
            },
            {
                case: "should throw exception on recursive input arrays",
                arguments: [
                    "myString",
                    54,
                    {},
                    null,
                    undefined
                ]
            }
        ];
        // one more input case to add - recursive arrays
        const inputArray = [4, 5, 9];
        inputArray.push(inputArray);

        badInputCases.push({
            case: "should throw exception on recursive input arrays",
            arguments: [inputArray]
        });

        return badInputCases;
}