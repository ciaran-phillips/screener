
const program = require('commander');

const CustomerInviteManager = require('./src/CustomerInviteManager.js');

program
    .usage("[options] <inputFile>")
    .arguments("<source> <destination>")
    .action(runProgram)
    .parse(process.argv);

function runProgram(source, destination) {
    const inviteManager = new CustomerInviteManager();

    inviteManager.read(source, (err) => {
        if (err) {
            console.error(err);
            process.exit(1);
        }
        inviteManager.write(destination, (err) => {
            if (err) {
                console.error(err);
                process.exit(1);
            }
            console.log('File written successfully');
        });
    });
}