module.exports = class CustomerListPreprocessor {
    /**
     * Does initial processing on the customer list 
     * 
     * @param {string} input
     *      string representation of the customer file
     * 
     * @throws CustomerInputError
     */
    constructor(input) {
        this._errors = [];
        
        this._customers = this._processInput(input);
    
    }

    /**
     * Get preprocessed customer data
     * 
     * @return {Object[]}
     *      Array of customer objects with the shape:
     *         { name: string, latitude: number, longitude: number, user_id: number }      
     */
    getCustomers() {
        return this._customers;
    }

    /**
     * Whether we hit any errors parsing the input 
     * 
     * @return {boolean}
     */
    hasErrors() {
        return (this._errors.length > 0);
    }

    /**
     * Get the list of errors for this input
     * 
     * @return {Object[]}
     *      Array of error objects with the shape:
     *          { line: number, message: string }
     */
    getErrors() {
        this._errors.sort((a, b) => {
            return this._errorSortCompare(a, b);
        });
        return this._errors;
    }

    /**
     * Parse and validate the customers list
     * 
     * @param {string} input
     *      The customer list
     * 
     * @return Object[]
     *      Array of customer objects
     */
    _processInput(input) {
        let customers = input.split(/\r?\n/);

        customers = customers.filter((customer) => {
            return customer.trim() !== "";
        });

        customers = customers.map((customer, index) => {
            try {
                customer = JSON.parse(customer);
                customer.latitude = parseFloat(customer.latitude);
                customer.longitude = parseFloat(customer.longitude);
                this._validate(customer, index);
                return customer;
            }
            catch (e) {
                if (e instanceof SyntaxError) {
                    this._errors.push(
                        `Line ${index}: invalid JSON`
                    );
                }
                else {
                    throw e;
                }
            }
        });
        return customers;
    }

    /**
     * Validate a customer object, checks fields are present and
     * correct datatypes. No return value - sets the errors array if needed
     * 
     * @param {Object} customer
     * @param {number} index
     */
    _validate(customer, index) {
        let errors = [
            this._checkForStringErrors(customer, "name"),
            this._checkForIntErrors(customer, "user_id"),
            this._checkForNumberErrors(customer, "latitude"),
            this._checkForNumberErrors(customer, "longitude")
        ];

        errors = 
            errors
                .filter((err) => { 
                    return (err !== null)
                })
                .map((err) => {
                    return `Line ${index}: ${err}`
                });
        this._errors = this._errors.concat(errors);
    }

    /**
     * Validates the given object field is a non-empty string
     * 
     * @param {Object} object
     * @param {field} string
     * 
     * @return {string|null}
     *      error message
     */
    _checkForStringErrors(object, field) {
        let err = null;
        if (typeof object[field] !== "string") {
            err = `${field} is not a string`;
        }
        else if (object[field].trim() === "") {
            err = `${field} is empty`;
        }
        return err;
    }

    /**
     * Validates the given object field is an integer
     * 
     * @param {Object} object
     * @param {field} string
     * 
     * @return {string|null}
     *      error message
     */
    _checkForIntErrors(object, field) {
        let err = null;
        if (!Number.isInteger(object[field])) {
            err = `${field} is not an integer`;
        }
        return err;
    }

    /**
     * Validates the given object field is a number
     * 
     * @param {Object} object
     * @param {field} string
     * 
     * @return {string|null}
     *      error message
     */
    _checkForNumberErrors(object, field) {
        let err = null;
        if (isNaN(parseFloat(object[field]))) {
            err = `${field} is not a number`;
        }
        return err;
    }

    /**
     * Compares error messages for sorting purposes (sorts alphabetically)
     * 
     * @param {Object} a
     * @param {Object} b
     * 
     * @return {number}
     */
    _errorSortCompare(a, b) {
        if (a < b) {
            return -1;
        }
        if (a > b) {
            return 1;
        }
        return 0;
    }
}