"use strict";

const EarthDistanceCalculator = require('./EarthDistanceCalculator.js');

module.exports = class InviteList {
    /**
     * Constructs an InviteList class, used to get list of customers 
     * eligible for invite
     * 
     * @param {Object[]} customerList
     *      Full list of customer objects
     * @param {EarthDistanceCalculator} distanceCalculator
     *      EarthDistanceCalculator dependency
     */
    constructor(customerList, distanceCalculator) {
        this._DUBLIN_LATITUDE = 53.3393;
        this._DUBLIN_LONGITUDE = -6.2576841;
        this._ELIGIBLE_DISTANCE = 100;

        if (!distanceCalculator) {
            distanceCalculator = new EarthDistanceCalculator();
        }
        this._EarthDistanceCalculator = distanceCalculator;

        this.customers = customerList;
    }

    /**
     * Get customers eligible for invite 
     * 
     * @return {Object[]}
     *      List of customer objects
     */
    getInvitees() {
        let customers = this.customers
            .filter((customer) => {
                return this._isEligible(customer);
            })
            .sort((a, b) => {
                return a.user_id - b.user_id;
            });
        return customers;
    }

    /**
     * Check if a given customer is eligible for invitation
     * (i.e. within 100km of the office)
     * 
     * @param {Object} customer
     *      Customer object with the shape 
     *      { name: string, latitude: number, longitude: number, user_id: number }
     * 
     * @return {boolean}
     */
    _isEligible(customer) {
        const calculator = this._EarthDistanceCalculator;
        const distance = calculator.distanceBetween(this._DUBLIN_LATITUDE, this._DUBLIN_LONGITUDE, 
            customer.latitude, customer.longitude);
        return (distance < this._ELIGIBLE_DISTANCE);
    }
}