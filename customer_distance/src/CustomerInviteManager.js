"use strict";

const fs = require('fs');
const CustomerListPreprocessor = require('./CustomerListPreprocessor.js');
const InviteList = require('./InviteList.js');

module.exports = class CustomerInviteManager {
    /**
     * Constructs a CustomerInviteManager instance
     * 
     * @param {object} fsModule
     *      fs dependency
     * @param {CustomerListPreprocessor} preprocessorClass
     *      List preprocessor dependency
     * @param {InviteList} inviteList
     *      InviteList dependency
     */
    constructor(fsModule, preprocessorClass, inviteListClass) {
        this._customersToInvite = [];
        
        if (!fsModule) {
            fsModule = fs;
        }
        this._fs = fsModule;

        if (!preprocessorClass) {
            preprocessorClass = CustomerListPreprocessor;
        }
        this._ListPreprocessor = preprocessorClass;

        if (!inviteListClass) {
            inviteListClass = InviteList;
        }
        this._InviteList = inviteListClass;
    }

    /**
     * Read and process the given customers file
     * 
     * @param {string} source
     *      file location
     * @param {function} callback
     *      Callback to be invoked after completion, with signature callback(err)
     */
    read(source, callback) {
        const options = {
            encoding: "utf8"
        };
        this._fs.readFile(source, options, (err, input) => {
            if (err) {
                callback(err);
                return;
            }
            this._getCustomersToInvite(input, callback);
            
        });
    }

    /**
     * Write the customer list to a new file
     * 
     * @param {string} destination
     *      The file to write to
     * @param {function} callback
     *      callback to be invoked after completion
     */
    write(destination, callback) {
        const customers = this._prepareCustomersForWrite(this._customersToInvite);
        this._fs.writeFile(destination, customers, (err) => {
            if (err) {
                callback(err);
            }
            callback(false);
        });
    }

    /**
     * Get the customers eligible for invites
     * 
     * @param {string} input
     *      The unprocessed customer list
     * @param {function} callback
     *      Callback to be invoked upon completion
     */
    _getCustomersToInvite(input, callback) {
        const preprocessor = new this._ListPreprocessor(input);

        if (preprocessor.hasErrors()) {
            callback(new Error(preprocessor.getErrors().join('\r\n')));
        }
        else {
            const customers = preprocessor.getCustomers();
            const inviteList = new this._InviteList(customers);

            this._customersToInvite = inviteList.getInvitees();
            callback();
        }
    }

    /**
     * Encodes the customer list as a newline separated list of JSON objects
     * 
     * @param {Object[]} customers
     *      Array of customer objects
     */
    _prepareCustomersForWrite(customers) {
        customers = customers
            .map((customer) => {
                return JSON.stringify(customer);
            })
            .join('\r\n');
        return customers;
    }

}