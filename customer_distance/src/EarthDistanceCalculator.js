"use strict";

module.exports = class EarthDistanceCalculator {
    
    constructor() {
        this._EARTH_RADIUS = 6371000;
    }

    /**
     * Get the Great Circle Distance between two points on Earth
     * 
     * @param {number} lat1
     * @param {number} lon1
     * @param {number} lat2
     * @param {number} lon2
     * 
     * @return {number}
     *      A float representing distance in KMs
     */
    distanceBetween(lat1, lon1, lat2, lon2) {
        lat1 = this._degreesToRadians(lat1);
        lon1 = this._degreesToRadians(lon1);
        lat2 = this._degreesToRadians(lat2);
        lon2 = this._degreesToRadians(lon2);
        const differenceInLongitude = Math.abs(lon1 - lon2);

        const cosOfLongitudeDifference = Math.cos(differenceInLongitude);
        const productOfSineLatitude = Math.sin(lat1) * Math.sin(lat2);
        const productOfCosLatitude = Math.cos(lat1) * Math.cos(lat2);

        const degreesBetweenPoints = Math.acos(productOfSineLatitude + (productOfCosLatitude * cosOfLongitudeDifference));

        const radiansBetweenPoints = (degreesBetweenPoints);

        const distanceInMetres = this._EARTH_RADIUS * radiansBetweenPoints;
        return distanceInMetres / 1000;
    }

    /**
     * Converts degress to radians
     * 
     * @param {number} degrees
     * 
     * @return {number}
     */
    _degreesToRadians(degrees) {
        return (degrees * Math.PI) / 180;
    }
}