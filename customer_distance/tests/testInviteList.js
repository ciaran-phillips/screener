const assert = require('assert');
const EarthDistanceCalculator = require('../src/EarthDistanceCalculator.js');
const sinon = require('sinon');

const InviteList = require('../src/InviteList.js');

describe('InviteList', function () {
    describe('#getInvitees()', function () {
        const testCases = testCaseData();

        it("should invite customers within 100km", () => {
            const mockCalculator = new EarthDistanceCalculator();
            const stub = sinon.stub(mockCalculator, 'distanceBetween');
            testCases.distances.forEach((dist, index) => {
                stub.onCall(index).returns(dist);
            });

            const inviteList = new InviteList(testCases.input, mockCalculator);
            const result = inviteList.getInvitees();
            assert.deepStrictEqual(result, testCases.expectedResult);
        });

        it("should order customers by ID ascending", () => {
            const mockCalculator = new EarthDistanceCalculator();
            const stub = sinon.stub(mockCalculator, 'distanceBetween');
            testCases.distances.forEach((dist, index) => {
                stub.onCall(index).returns(dist);
            });

            const inviteList = new InviteList(testCases.inputUnordered, mockCalculator);
            const result = inviteList.getInvitees();
            assert.deepStrictEqual(result, testCases.expectedResult);

        })

    });
});


function testCaseData() {
    // lat & long isn't actually expected to change the outcome here,
    // since we're stubbing the distance method
    const input = [
        {
            latitude: 11.43,
            longitude: -11.2,
            user_id: 1
        },
        {
            latitude: 22.43,
            longitude: -22.2,
            user_id: 2
        },
        {
            latitude: 33.43,
            longitude: -33.2,
            user_id: 3
        },
        {
            latitude: 44.43,
            longitude: -44.2,
            user_id: 4
        }
    ];

    const inputUnordered = [
        {
            latitude: 33.43,
            longitude: -33.2,
            user_id: 3
        },
        {
            latitude: 22.43,
            longitude: -22.2,
            user_id: 2
        },
        {
            latitude: 11.43,
            longitude: -11.2,
            user_id: 1
        },
        {
            latitude: 44.43,
            longitude: -44.2,
            user_id: 4
        }
    ];

    const distances = [99.5, 100, 0, 1000];

    // based on the distances we're passing into our mock, 
    // we want the first and third customers to be returned
    const expectedResult = [
        {
            latitude: 11.43,
            longitude: -11.2,
            user_id: 1
        },
        {
            latitude: 33.43,
            longitude: -33.2,
            user_id: 3
        }
    ];

    return {
        input,
        inputUnordered,
        expectedResult,
        distances
    }

}