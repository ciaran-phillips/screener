const assert = require('assert');

const CustomerListPreprocessor = require('../src/CustomerListPreprocessor.js');

describe('CustomerListPreprocessor', function () {
    describe('#getCustomers()', function () {

        it("should split the input file on line breaks and return a list of objects", () => {
            const testData = validInput();
            const preprocessor = new CustomerListPreprocessor(testData.input);
            assert.ok(!preprocessor.hasErrors());

            const result = preprocessor.getCustomers();
            assert.deepStrictEqual(result, testData.expectedOutput);
        });


        it("should report an error for every issue with the input file", () => {
            const testData = invalidInput();
            const preprocessor = new CustomerListPreprocessor(testData.input);
            assert.ok(preprocessor.hasErrors());
            assert.deepStrictEqual(preprocessor.getErrors(), testData.expectedErrors);
        })

    });
});

/**
 * Return valid test data 
 * 
 * @return {Object}
 *      Test case data with the shape { input: string, expectedErrors: Object[] }
 */
function validInput() {
    const input = `
        {"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}
        {"latitude": "51.92893", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}
        {"latitude": "51.8856167", "user_id": 2, "name": "Ian McArdle", "longitude": "-10.4240951"}
        `;
    const expectedOutput = [
        {"latitude": 52.986375, "user_id": 12, "name": "Christina McArdle", "longitude": -6.043701},
        {"latitude": 51.92893, "user_id": 1, "name": "Alice Cahill", "longitude": -10.27699},
        {"latitude": 51.8856167, "user_id": 2, "name": "Ian McArdle", "longitude": -10.4240951}
    ];

    return {
        input,
        expectedOutput
    };
}

/**
 * Return invalid test data, and expected getErrors
 * 
 * @return {object}
 *      Test case data with the shape { input: string, expectedErrors: Object[] }
 */
function invalidInput() {
    const input = `
        {"latitude": "52.986375", "user_id": "five", "name": "", "longitude": ""}
        {"latitude": "51.92893", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}
        {"latitude": "51.8856167", "user_id": null, "name": "Ian McArdle", "longitude": "-10.4240951"}
        {"latitude": "51.8856167" "user_id": null "name": "Ian McArdle", "longitude": "-10.4240951"}
        `;
    const expectedErrors = [
        "Line 0: longitude is not a number",
        "Line 0: name is empty",
        "Line 0: user_id is not an integer",
        "Line 2: user_id is not an integer",
        "Line 3: invalid JSON" 
    ];

    return {
        input,
        expectedErrors
    };
}