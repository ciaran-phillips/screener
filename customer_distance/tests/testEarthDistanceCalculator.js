const assert = require('assert');

const EarthDistanceCalculator = require('../src/EarthDistanceCalculator.js');

describe('EarthDistanceCalculator', function () {
    describe('#distanceBetween()', function () {
        const cases = [
            {
                lat1: 53.3393,
                lon1: -6.2576841,
                lat2: 52.986375,
                lon2: -6.043701,
                distance: 41.756
            },
            {
                lat1: 53.3393,
                lon1: -6.2576841,
                lat2: 54.0894797,
                lon2: 6.18671,
                distance: 822.08
            },
            {
                lat1: 51.3393,
                lon1: -4.2576841,
                lat2: 57.0894797,
                lon2: 5.18671,
                distance: 885
            },
            {
                lat1: -53.3393,
                lon1: 6.2576841,
                lat2: 51.802,
                lon2: -9.442,
                distance: 11612.17
            },
        ];
        const distanceCalc = new EarthDistanceCalculator();

        it("should calculate the distance in KMs to within 2% margin of error", function() {
            cases.forEach(function(test) {
                const result = distanceCalc.distanceBetween(test.lat1, test.lon1, test.lat2, test.lon2);
                // check we're within a 2% margin of error 
                // (on larger distances we seem to be going up to 1.5% out, 
                // as compared to http://www.convertalot.com/great_circle_distance_calculator.html)
                // Possibly due to using a different formula or taking different values for Earth's radius
                const difference = Math.abs(result - test.distance);
                const marginOfError = (difference / test.distance) * 100;
                const withinOnePercentMargin = marginOfError < 2;
                assert.ok(withinOnePercentMargin);
            });
        });
        
    });
});
