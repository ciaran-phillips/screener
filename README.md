## Array flattener

* Code is in the array_flattener folder
* Tests can be run using `npm install` and `npm run test` within that folder

## Customer Invites

* Code is in the customer_distance folder
* Tests can be run using `npm install` and `npm run test` within the folder
* It can process a customers file using `node index.js SOURCE_FILE DESTINATION_FILE`
* Running `node index.js --help` shows the command's documentation


## Achievement Question

* I've written up an answer in question_one_answer.txt in the root folder 
